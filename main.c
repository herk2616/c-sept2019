#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Arrays */
struct camper_dat {
	char firstname[20];	//
	char lastname[20];	// An array of
	int id;			// structs for
	int birth_year;		// storing all
	char gender;		// the information
}camper[120];
int seat[100];			// Array for the seats

/* Options */
int time_fast = 1;		// Used in
int time_normal = 3;		// Clear() to
int time_slow = 5;		// set delay

/*  Functions */
//HF
int Gui(char title[], char firstline[], char secondline[], char thirdline[], char forthline[], char fifthline[]);
int Camper_check(struct camper_dat camper[120]);
int Seat_check(int seat[100], int index);
int Print_seat_array(int seat[100]);
char Secure_MF();
int Secure_ID();
int Secure_int(int floor, int sealing);
void Clear(int delay);

//CF
int Login();
int Register();
int Camper_screen();
int Personal_screen(int index);
int View(int index);
int Book(int index);
int Random(int index);
int Modify(int index);

//AF
int Admin_screen_1();
int Admin_screen_2();
int Admin_view();
int Admin_search();
int Admin_delete();
int Admin_minigift();
int Admin_megagift();
int Admin_sort();

//SF
int Sort_swap(int index_1, int index_2);
int Sort_lastname();
int Sort_birth_year();
int Sort_gender();
int Array_copy(char array_1[20], char array_2[20]);


/*  Main  */
int main(void){
	char user_input[20];

	while(1){
		Clear(0);
		Gui("\t{ Demoup Camping }\t", "\t\t\t\t", " > admin\t\t\t", "\t\t\t\t", " > camp\t\t\t", "\t\t\t\t");
		scanf("%s",&user_input);
		if (strcmp(user_input, "admin") == 0){
			Clear(0);
			Admin_screen_1();

		} else if (strcmp(user_input, "camp") == 0){
			Clear(0);
			Camper_screen();
		}
	}
	return 0;
}	// End of main


/* Camper screen */
int Camper_screen(){
	int camper_screen = 1;
	int login_index;
	char user_input[20];

	while( camper_screen != 0 ){

		Gui("\t{ Demup Camping }\t", "\t\t\t\t", " > Register\t\t\t", "\t\t\t\t", " > Login\t\t\t", "\t\t\t\t");
		scanf("%s",&user_input);
		if (strcmp(user_input, "Register") == 0){
			Register();
			Clear(0);
			camper_screen = 0;
		}
		else if (strcmp(user_input, "Login") == 0){
			login_index = Login(camper);
			if(login_index >= 0){				// If login is corect, login_index is users
				Personal_screen(login_index);		// camper-array index and it gets passed
				Clear(0);
			}
			camper_screen = 0;
		}
		else{
			Clear(0);
			camper_screen = 1;
		}
	}
	return 0;
}	// End of Camper screen


/* Personal_screen */
int Personal_screen(int index){
	int personal_screen = 1;
	char user_input[20];

	while( personal_screen != 0 ){
		Gui("\t{ Personal screen }\t", " > View\t\t\t", " > Book\t\t\t", " > Random\t\t\t", " > Modify\t\t\t", " > Exit\t\t\t");
		scanf("%s",&user_input);
		printf("\n\n");

		if (strcmp(user_input, "View") == 0){
			View(index);
			Clear(time_normal);
			personal_screen = 1;

		} else if (strcmp(user_input, "Book") == 0){
			Book(index);
			Clear(time_normal);
			personal_screen = 1;

		} else if (strcmp(user_input, "Random") == 0){
			Random(index);
			Clear(time_normal);
			personal_screen = 1;

		} else if (strcmp(user_input, "Modify") == 0){
			Modify(index);
			Clear(0);
			personal_screen = 1;

		} else if (strcmp(user_input, "Exit") == 0){
			personal_screen = 0;
		} else {
			Clear(0);
			personal_screen = 1;
		}
	}
	return 0;
}	// End of personal screen


/* GUI */
int Gui( char title[], char firstline[], char secondline[], char thirdline[], char forthline[], char fifthline[]){
	printf("+-------------------------------+\n|\033[32m%s\033[0m|\n|\t\t\t\t|\n|%s|\n|%s|\n|%s|\n|%s|\n|%s|\n|\t\t\t\t|\n+-------------------------------+\n[\033[32mConsole ~>\033[0m] ",
			title,firstline,secondline,thirdline,forthline,fifthline);	//Well.. that went over the 90 character mark..
	return 0;
}	// End of GUI


/* Camper check */
int Camper_check(struct camper_dat camper[120]){
	int camper_index;				// Checks the camper array for
	int i=0;					// available spots and returns
							// the index of the first.
	for( i=0 ; i<120 ; i++){
		if( camper[i].id == 0 ){
			camper_index = i;
			break;
		} else {
			camper_index = -1;
		}
	}
	return camper_index;
}	// End of camper check


/* Seat_check */
int Seat_check(int seat[100], int index){
	int i=0;
	int seat_num;

	for(i=0;i<100;i++){
		if (seat[i] == camper[index].id){	// Check if there is already a seat assign to users id
			seat_num = i;
			break;
		} else {
			seat_num = -1;
		}
	}
	return seat_num;
}	// End of Seat check

/* Secure Int */
int Secure_int(int floor, int sealing){
	int secure_int=1;
	int user_int=-1;
	char garbage;

	while(secure_int != 0){
		scanf("%d",&user_int);
		scanf("%c",&garbage);
		if((user_int >= floor) && (user_int <= sealing)){
			secure_int = 0;
		} else {
			printf("\n[\033[31m Enter a number %d-%d\033[0m ]", floor, sealing, user_int);
			secure_int = 1;
		}
	}
		return user_int;
}	//End of Secure int


/* Secure_MF */
char Secure_MF(){
	char user_gender;
	int secure_mf=1;

	while(secure_mf !=0 ){
		scanf("%s",&user_gender);
		switch (user_gender){
			case 'M': secure_mf = 0; break;
			case 'F': secure_mf = 0; break;
			default : printf("\n[\033[32mEnter F or M ~> \033[0m]"); secure_mf = 1; break;
		}
	}
	return user_gender;
}	//End of secure mf


/* Secure ID */
int Secure_id(){
	int secure_id=1;
	int user_id;
	int i=0;

	while(secure_id !=0 ){
		user_id = Secure_int(1,2147483646);
		for(i=0;i<120;i++){
			if(camper[i].id == user_id){
			printf("\n[\033[31mDuplicate id found, Please try again or contact local admin\033[0m ] : "); secure_id = 1; break;
			break;
			} else if( i == 119 ){
				secure_id = 0;
			}
		}
	}
	return user_id;
}	//End of secure id




/* Print seat array */
int Print_seat_array(int seat[100]){
	int i=0;					// OK 20 rows is retarded so if you want 20
	int j=0;					// just change the width var to 20
	int width=10;					//(anything that can divide 100 perfectly can be used)

	for (i=0 ; i < 100 ; i += width){
		for (j=0 ; j < width ; j++){
			if ( seat[i+j] == 0 ){
				printf("\033[32m%d\033[0m\t",i+j);
			} else {
				printf("\033[31m0\033[0m\t");
			}
		}
		printf("\n");
	}
	printf("\n");
	return 0;
}	//End of Print seat array


/* Camper Functions */

/* Register */
int Register(){
	int camper_index = Camper_check(camper);		// I am using the Gui for a nice interface

	if ( camper_index >= 0 ){			// camper_index is set from Camper_check
		Clear(0);
		Gui("\t{Register Screen}\t", " > Name {__}\t\t\t", "\t\t\t\t", "\t\t\t\t", "\t\t\t\t", "\t\t\t\t");
		scanf("%s",&camper[camper_index].firstname);
		Clear(0);
		Gui("\t{Register Screen}\t", " > Name {OK}\t\t\t", " > Last name {__}\t\t", "\t\t\t\t", "\t\t\t\t", "\t\t\t\t");
		scanf("%s",&camper[camper_index].lastname);
		Clear(0);
		Gui("\t{Register Screen}\t", " > Name {OK}\t\t\t", " > Last name {OK}\t\t", " > Id {__}\t\t\t", "\t\t\t\t", "\t\t\t\t");
		camper[camper_index].id = Secure_id();
		Clear(0);
		Gui("\t{Register Screen}\t", " > Name {OK}\t\t\t", " > Last name {OK}\t\t", " > Id {OK}\t\t\t", " > Year of birth {__}\t\t", "\t\t\t\t");
		camper[camper_index].birth_year = Secure_int(1940,2019);
		Clear(0);
		Gui("\t{Register Screen}\t", " > Name {OK}\t\t\t", " > Last name {OK}\t\t", " > Id {OK}\t\t\t", " > Year of birth {OK}\t\t", " > Gender {__}\t\t\t");
		camper[camper_index].gender = Secure_MF();
		Clear(0);
		Gui("\t{Register Screen}\t", " > Name {OK}\t\t\t", " > Last name {OK}\t\t", " > Id {OK}\t\t\t", " > Year of birth {OK}\t\t", " > Gender {OK}\t\t\t");
		printf("\n");
	} else {
		Clear(time_normal);
		Gui("\t{Register Screen}\t", "\033[31m Camp full\033[0m\t\t\t", "\t\t\t\t", "\t\t\t\t", "\t\t\t\t", "\t\t\t\t");
	}
}	// End of register


/* Login */
int Login(){
	int i=0;
	int login_index;
	int user_id;
	int user_birth_year;

	Clear(0);
	Gui("\t{ Login }\t\t", "\t\t\t\t", " > username {__}\t\t", "\t\t\t\t", "\t\t\t\t", "\t\t\t\t");
	scanf("%d",&user_id);
	printf("\n\n");
	for( i=0 ; i<120 ; i++){
		if(camper[i].id == user_id ){				// If the id exist
			Clear(0);
			Gui("\t{ Login }\t\t", "\t\t\t\t", " > username {OK}\t\t", "\t\t\t\t", " > password {__}\t\t", "\t\t\t\t");
			scanf("%d",&user_birth_year);
			printf("\n\n");
			if(camper[i].birth_year == user_birth_year){	// And the password is correct
				Clear(0);
				Gui("\t{ Login }\t\t", "\t\t\t\t", " > username {OK}\t\t", "\t\t\t\t", " > password {OK}\t\t", "\t\t\t\t");
				printf("\n\n");
				Clear(time_fast);
				Gui("\t{ Login }\t\t", "\t\t\t\t", " Welcome user!\t\t\t", "\t\t\t\t", "\t\t\t\t", "\t\t\t\t");
				printf("\n\n");
				Clear(time_fast);
				login_index = i;			// Login_index is set to whoever's-account-is-matching camper-array index
			} else {					// Password incorrect
				Clear(0);
				Gui("\t{ Login }\t\t", "\t\t\t\t", " > username {OK}\t\t", "\t\t\t\t", " > password {OK}\t\t", "\t\t\t\t");
				printf("\n\n");
				Clear(time_fast);
				Gui("\t{ Login }\t\t", "\t\t\t\t", "\033[31m  Error\033[0m \t\t\t", "incorrect username or password\t", "\t\t\t\t", "\t\t\t\t");
				printf("\n\n");
				Clear(time_normal);
				login_index = -2;
			}
			break;
		} else {						// Username not found
				login_index = -3;
		}
	}
	if(login_index == -3){						// We want the same responce for every case
		Clear(0);
		Gui("\t{ Login }\t\t", "\t\t\t\t", " > username {OK}\t\t", "\t\t\t\t", " > password {__}\t\t", "\t\t\t\t");
		scanf("%d",&user_birth_year);
		printf("\n\n");
		Clear(0);
		Gui("\t{ Login }\t\t", "\t\t\t\t", " > username {OK}\t\t", "\t\t\t\t", " > password {OK}\t\t", "\t\t\t\t");
		printf("\n\n");
		Clear(time_fast);
		Gui("\t{ Login }\t\t", "\t\t\t\t", "\033[31m  Error\033[0m \t\t\t", "incorrect username or password\t", "\t\t\t\t", "\t\t\t\t");
		printf("\n\n");
		Clear(time_normal);
		login_index = -2;
	}
	return login_index;
}	// End of login


/* View */
int View(int index){		// Similar to Gui but for printing camper values only
	printf("+-------------------------------+\n|\t{ View menu }\t\t\n|\n| 1) Name       : %s\t\t\n| 2) Surname    : %s\n| 3) Id         : %d\n| 4) Birth year : %d \n| 5) Gender     : '%c'\n|\n+-------------------------------+\n",
			camper[index].firstname,camper[index].lastname,camper[index].id,camper[index].birth_year,camper[index].gender);
	return 0;
}	//	End of View


/* Book */
int Book(int index){
	int user_seat=-1;
	int i=0;

	Clear(0);
	Print_seat_array(seat);
	if(Seat_check(seat,index) > 0){						// If he already has a seat
		printf("[\033[32m You already have a seat\033[0m ]\n");		// echo and leave.
	} else {
		while( user_seat == -1 ){					// If he hasn't a seat
		printf("[\033[32m Please select a seat number ~>\033[0m]");
		user_seat = Secure_int(0,99);
		printf("\n");
			if( seat[user_seat] == 0 ){				// select a valid seat
				seat[user_seat] = camper[index].id;		// assign seat to user
				Clear(0);
				Print_seat_array(seat);
				printf("[\033[32m Seat assigned\033[0m ]\n");	// echo and leave.
				break;
			} else {
				user_seat = -1;					// Else loop back up
			}
		}
	}

	return 0;
}	// End of book


/* Random */
int Random(int index){
		int i=0;

	Clear(0);
	Print_seat_array(seat);
	if (Seat_check(seat,index) >= 0){						// If he already has a seat
		printf("[\033[32m You already have a seat\033[0m ]\n");			// echo and leave.
	} else {
		if (camper[index].gender == 'M'){
			for (i=0;i<50;i++){
				if (seat[i] == 0){
					seat[i] = camper[index].id;
					Clear(time_fast);
					Print_seat_array(seat);
					printf("[\033[32m Seat assigned\033[0m ]\n");
					break;
				}
			}
		} else if (camper[index].gender == 'F'){
			for (i=50;i<100;i++){
				if (seat[i] == 0){
					seat[i] = camper[index].id;
					Clear(time_fast);
					Print_seat_array(seat);
					printf("[\033[32m Seat assigned\033[0m ]\n");
					break;
				}
			}
		}
	}
	return 0;
}	// End of random


/* Modify */
int Modify(int index){
	int user_select = 0;
	int modify = 1;

	View(index);
	while( modify != 0 ){
		printf("[\033[32m Select ~>\033[0m ]");
		user_select = Secure_int(1,5);
		printf("\n");
		switch(user_select){
			case 1 : printf("[\033[32m Enter new name\033[0m ]");
				 scanf("%s",&camper[index].firstname); modify = 0;
				 break;
			case 2 : printf("[\033[32m Enter new surname\033[0m ]");
				 scanf("%s",&camper[index].lastname); modify = 0;
				 break;
			case 3 : printf("[\033[32m Enter new id\033[0m ]");
				 camper[index].id = Secure_id(); modify = 0;
				 break;
			case 4 : printf("[\033[32m Enter new birth year\033[0m ]");
				 camper[index].birth_year = Secure_int(1940,2019); modify = 0;
				 break;
			case 5 : printf("[\033[32m Enter new gender\033[0m ]");
				 camper[index].gender = Secure_MF(); modify = 0;
				 break;
			default : modify = 1; user_select = 0; break;
		}
		printf("\n");
	}
	return 0;
}	// End of Modify


/* Admin Functions */

/* Admin_screen_1 */
int Admin_screen_1(){
	int admin_screen;
	char user_input[20];

	while( admin_screen != 0 ){
		Gui("\t{ Admin screen 1 }\t", " > View\t\t\t", " > Search\t\t\t", " > Delete\t\t\t", " > Next\t\t\t", " > Exit\t\t\t");
		scanf("%s",&user_input);
		printf("\n");
		if (strcmp(user_input, "View") == 0){
			Admin_view();
			Clear(time_slow);
			admin_screen = 1;

		} else if (strcmp(user_input, "Search") == 0){
			Admin_search();
			Clear(time_normal);
			admin_screen = 1;

		} else if (strcmp(user_input, "Delete") == 0){
			Admin_delete();
			Clear(time_fast);
			admin_screen = 1;

		} else if (strcmp(user_input, "Next") == 0){
			Clear(0);
			Admin_screen_2();
			admin_screen = 1;

		} else if (strcmp(user_input, "Exit") == 0){
			Clear(0);
			admin_screen = 0;
		} else {
			Clear(0);
			admin_screen = 1;
		}
	}
	return 0;
}	// End of admin screen


/* Admin_screen_2 */
int Admin_screen_2(){
	int admin_screen;
	char user_input[20] = {"null"};

	while( admin_screen != 0 ){
		Gui("\t{ Admin screen 2 }\t", " > Minigift\t\t\t", " > Megagift\t\t\t", " > Sort\t\t\t", " > Back\t\t\t", " > Exit\t\t\t");
		scanf("%s",&user_input);
		printf("\n");
		if (strcmp(user_input, "Minigift") == 0){
			Admin_minigift();
			Clear(time_slow);
			admin_screen = 1;

		} else if (strcmp(user_input, "Megagift") == 0){
			Admin_megagift();
			Clear(time_normal);
			admin_screen = 1;

		} else if (strcmp(user_input, "Sort") == 0){
			Clear(0);
			Admin_sort();
			admin_screen = 1;

		} else if (strcmp(user_input, "Back") == 0){
			Clear(0);
			admin_screen = 0;

		} else if (strcmp(user_input, "Exit") == 0){
			Clear(0);
			admin_screen = 0;
		} else {
			Clear(0);
			admin_screen = 1;
		}
	}
	return 0;
}	// End of admin screen


/* Admin_view */
int Admin_view(){
	int male=0;
	int female=0;
	int i=0;

	for(i=0;i<120;i++){
		if(camper[i].id > 0 ){
			if(camper[i].gender == 'M'){
				male++;
			} else {
				female++;
			}
			printf("[\033[31m INDEX NUM \033[0m : \033[32m%d\033[0m ] [\033[31m Seat \033[0m : \033[32m%d\033[0m ]\n",
					i, Seat_check(seat,i));
			View(i);
		}
	}
	printf("[\033[31m males\033[0m : %d ] [\033[31m females\033[0m : %d ] [\033[31m total\033[0m : %d ]\n\n",male,female,male+female);
	return 0;
}	// End of admin view


/* Admin_search */
int Admin_search(){
	int i=0;
	char user_char;

	printf("[\033[32m Enter character ~>\033[0m ]");
	scanf("%s",&user_char);
	for(i=0;i<120;i++){
		if(camper[i].lastname[0] == user_char){			// I was debating to use tostring to make it
			View(i);					// case insensitive, But i had to use an extra
		}							// library for it, so .. no tostring
	}
	return 0;
}	// End of admin search


/* Admin_delete */
int Admin_delete(){
	int default_to = 0;				// OK we could set all values to -1, but that would just waste
	int i=0;					// array space, if we set them to 0 then Register() can override them
	int j=0;					// Again if we reaaly need to set them to -1 just change the default_to
	char yes_no;
	char user_input[20];

	printf("[\033[32m Enter Last name ~>\033[0m ]");
	scanf("%s",&user_input);
	printf("\n");
	for(i=0;i<120;i++){
		if (strcmp(user_input, camper[i].lastname) == 0){
			Clear(0);
			View(i);
			printf("[\033[31m Are you sure you want to continue ?\033[0m Y/n ]");
			scanf("%s",&yes_no);
			switch(yes_no){
				case 'Y':
					camper[i].firstname[0]=default_to;
					camper[i].lastname[0]=default_to;
					camper[i].id=default_to;
					camper[i].birth_year=default_to;
					camper[i].gender=default_to;
					for(j=0;j<100;j++){
						if(seat[j] == camper[i].id){		// Checking all of the array
							seat[j] = 0;			// in case of multiple seats
						}					// I intentionally didn't put break
					}
					printf("[\033[31m Deletion completed\033[0m ]\n");
					break;
				case 'N':
				case 'n':
				deafault: printf("[\033[31m Deletion aborted\033[0m ]\n");
			}
			break;
		}
	}
	return 0;
}	// End of admin delete


/* Admin_minigift */
int Admin_minigift(){
	int i=0;
	int j=0;
	int win_dig;
	int loser = 1;


	printf("[\033[32m Select last digit\033[0m ]");
	win_dig = Secure_int(0,9);
	for(i=0;i<120;i++){
		if(((camper[i].id - win_dig) % 10 == 0) && (camper[i].id != 0)){	// Herk's theorm
			View(i);							// ∀ x ∈ N ∃ y : x*10-y % = 0
			for (j=0;j<100;j++){						// But we also don't want ids == 0
				if (seat[j] == 0){					// Even though id == 0 is literally
					seat[j] = camper[i].id;				// the default of seat[] so it doesn't
					printf("[\033[32m Seat assigned\033[0m ]\n");	// do anything other than printing
					loser = 0;
					break;
				} else {
					loser = 1;
				}
			}
		}
	}
	if(loser == 1){
		printf("[\033[31m No id found ending in %d\033[0m ]\n",win_dig);
	}
	return 0;
}	// End of admin minigift


/* Admin_megagift */
int Admin_megagift(){
	int i=0;
	int random_index;
	int winner_seat;

	random_index = rand() % 100;							// Random number 0-99
	if(seat[random_index] > 0){							// If seat exist
		printf("[\033[32m Found random winner\033[0m ]\n");			// echo winner
		winner_seat = random_index;						// and set winner_seat var
	} else {
		Clear(0);
		printf("[\033[31m Randomly selected seat wasn't reserved (seat %d)\033[0m ]\n",random_index);  // Else select manually
		Print_seat_array(seat);
		printf("[\033[32m Select manually a seat\033[0m ]");
		winner_seat = Secure_int(0,99);
	}
	if(seat[winner_seat] == 0){							// If id==0
		printf("\n[\033[31m None is assigned in this seat\033[0m ]\n");		// seat isn't assigned
		Clear(time_fast);
	} else {
		Clear(0);								// Else find winners
		printf("\n[\033[32m Winner is\033[0m ]\n");				// camper-array index
		for(i=0;i<120;i++){
			if(camper[i].id == seat[winner_seat]){
					break;
					}
				}
		View(i);								// and print winner details
		printf("[\033[32m You should probably want to write this down\033[0m ]\n\n");
	}
	return 0;
}	// End of admin megagift


/* Admin_sort */
int Admin_sort(){
	int sort_screen;
	char user_input[20] = {"null"};

	while( sort_screen != 0 ){
	Gui("\t{ Sort screen }\t\t", " > Surname\t\t\t", " > Birth\t\t\t", " > Gender\t\t\t", "\t\t\t\t", " > Exit\t\t\t");
		scanf("%s",&user_input);
		printf("\n");

		if (strcmp(user_input, "Surname") == 0){
			Sort_lastname();
			Admin_view();
			Clear(time_normal);
			sort_screen = 1;

		} else if (strcmp(user_input, "Birth") == 0){
			Sort_birth_year();
			Admin_view();
			Clear(time_normal);
			sort_screen = 1;

		} else if (strcmp(user_input, "Gender") == 0){
			Sort_gender();
			Admin_view();
			Clear(time_normal);
			sort_screen = 1;

		} else if (strcmp(user_input, "Exit") == 0){
			Clear(0);
			sort_screen = 0;
		} else {
			Clear(0);
			sort_screen = 1;
		}
	}
	return 0;
}	// End of admin sort


/* Sort_gender */
int Sort_gender(){
	int i=0;							// OK 'F' is ASCII code 70 and 'M' is 77
	int j=0;							// so it will sort female first

	for(i=0;i<120 - 1 ;i++){					// but all all the empty spots are '0' so everything
		for(j=0;j<120 - i - 1;j++){				// will move to the bottom of the array
			if( camper[j].gender > camper[j+1].gender ){	// which is good, because we set the campers from top to
				Sort_swap(j, j+1);			// bottom, so it will take less steps to assign a camper
			}
		}
	}
	return 0;
}	// End of sort gender


/* Sort_lastname */
int Sort_lastname(){
	int i=0;							// Same as before but with the first
	int j=0;							// digit of campers.lastname

	for(i=0;i<120 - 1 ;i++){
		for(j=0;j<120 - i - 1;j++){
			if( camper[j].lastname[0] > camper[j+1].lastname[0] ){
				Sort_swap(j, j+1);
			}
		}
	}
	return 0;
}	// End of sort lastname


/* Sort_birth_year */
int Sort_birth_year(){
	int i=0;							// Same as before but with actual
	int j=0;							// numbers that have a meaning

	for(i=0;i<120 - 1 ;i++){
		for(j=0;j<120 - i - 1;j++){
			if( camper[j].birth_year > camper[j+1].birth_year ){
				Sort_swap(j, j+1);
			}
		}
	}
	return 0;
}	// End of sort birth year


/* Sort_swap */
int Sort_swap(int index_1, int index_2){
	char temp_firstname[20];					// Swaps struct-array values, used in bubble sort
	char temp_lastname[20];						// {temp}     <- {values_1}
	int temp_id;							// {values_1} <- {values_2}
	int temp_birth_year;						// {values_2} <- {temp}
	char temp_gender;

	Array_copy( temp_firstname, camper[index_1].firstname);
	Array_copy( temp_lastname, camper[index_1].lastname);
	temp_id = camper[index_1].id;
	temp_birth_year = camper[index_1].birth_year;
	temp_gender = camper[index_1].gender;

	Array_copy( camper[index_1].firstname, camper[index_2].firstname);
	Array_copy( camper[index_1].lastname, camper[index_2].lastname);
	camper[index_1].id = camper[index_2].id;
	camper[index_1].birth_year = camper[index_2].birth_year;
	camper[index_1].gender = camper[index_2].gender;

	Array_copy( camper[index_2].firstname, temp_firstname);
	Array_copy( camper[index_2].lastname, temp_lastname);
	camper[index_2].id = temp_id;
	camper[index_2].birth_year = temp_birth_year;
	camper[index_2].gender = temp_gender;
}	// End of sort swap


/* Array_copy */
int Array_copy(char array_1[20], char array_2[20]){
	int i=0;							// Copies a string to another
	for(i=0;i<20;i++){						// We deal with fixed-length arrays so
		array_1[i] = array_2[i];				// this will not fail (hopefully)
	}								// used in Sort_swap
	return 0;
}	// End of array copy



/* Clear screen */
void Clear(int delay){
	sleep(delay);
	system("cls||clear");						// If cls fails (Windows) use clear (normal OS)
}	// End of clear

